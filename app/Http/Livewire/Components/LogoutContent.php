<?php

namespace App\Http\Livewire\Components;

use Livewire\Component;

class LogoutContent extends Component
{
    public function render()
    {
        return view('livewire.components.logout-content');
    }
    public function logout()
    {
        Auth::logout();
        session()->flash('success', 'ອອກຈາກລະບົບສຳເລັດ!');
        return redirect(route('login'));
    }
}