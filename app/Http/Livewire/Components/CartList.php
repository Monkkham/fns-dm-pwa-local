<?php

namespace App\Http\Livewire\Components;

use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithFileUploads;

class CartList extends Component {
    use WithFileUploads;
    public $cartData;
    public $cartCount;
    public $cartTotal;
    public $foods;
    public $zones;
    public $table;
    public $checkold;

    public function render() {
        $this->cartData = Cart::content();
        $this->cartCount = Cart::count();
        $this->cartTotal = Cart::subTotal();
        $this->foods = DB::table( 'foods' )->get();
        $this->zones = DB::table( 'zones' )->select( '*' )->get();
        $this->table = DB::table( 'tables' )->where( 'status', 0 )->limit( 4 )->get();
        $this->checkold = DB::table( 'preorder' )->where( 'customer_id', auth()->user()->id )->where( 'status', '!=', 6 )->get();
        return view( 'livewire.components.cart-list' )->layout( 'layouts.auth.style' );
    }
    // add txiv hauv qub
    public $oldId;

    public function _checadd() {
        $this->validate( [
            'oldId' => 'required',
        ] );
        DB::beginTransaction();
        // try {
            $preorderId = DB::table( 'preorder' )->where( 'id', $this->oldId )->first()->id;
            $cartContent = Cart::content();
            foreach ( $cartContent as $item ) {
                if ( isset( $item->id ) ) {
                    DB::table( 'preorder_food' )->insert( [
                        'preorder_id' => $preorderId,
                        'foods_id' => $item->id,
                        'food_qty' => $item->qty,
                        'subtotal' => str_replace( ',', '', Cart::subTotal() ),
                    ] );
                }
            }
            DB::commit();
            Cart::destroy();
            toastr()->success( 'successfully!' );
            return redirect()->route( 'history' );
        // } catch ( \Exception $e ) {
        //     DB::rollback();
        //     toastr()->error( 'error someting!' );
        // }

    }
    // update cart
    public $rowId, $qty;

    public function updateCart( $qty, $rowId, $status ) {
        if ( $status == 1 ) {
            $this->rowId = $rowId;
            $this->qty = $qty + 1;
            Cart::update( $this->rowId, $this->qty );
            toastr()->success( 'successfully!' );
            return;
        } else {
            $this->rowId = $rowId;
            $this->qty = $qty - 1;
            Cart::update( $this->rowId, $this->qty );
            toastr()->success( 'successfully!' );
        }
    }

    public $chosetable = [];
    public $qtyp, $datetime, $payment;

    function _checkout( $status ) {
        $this->validate( [
            'chosetable' => 'required',
            'qtyp' => 'required',
            'datetime' => 'required',
            'payment' => 'required',
        ] );
        $forCheck = str_replace( ',', '', Cart::subTotal() );
        if ( $status == 1 ) {
            if ( $forCheck >= 1 ) {
                $order = DB::table( 'preorder' )->insertGetId( [
                    'customer_id' => auth()->user()->id,
                    'code' =>'OD-'. rand( 100000, 999999 ),
                    'subtotal' => str_replace( ',', '', Cart::subTotal() ),
                    'total' => str_replace( ',', '', Cart::subTotal() ),
                    'onepay_image' => 'https://www.pngitem.com/pimgs/m/279-2797802_payment-download-png-image-pagamento-recorrente-transparent-png.png',
                    'qtyp' => $this->qtyp,
                    'datetime' => $this->datetime,
                    'payment' => 1,
                    'status' => 1,
                ] );
            } else {
                $order = DB::table( 'preorder' )->insertGetId( [
                    'customer_id' => auth()->user()->id,
                    'code' =>'OD-'. rand( 100000, 999999 ),
                    'subtotal' => 50000,
                    'total' => str_replace( ',', '', Cart::subTotal() ),
                    'onepay_image' => 'https://www.pngitem.com/pimgs/m/279-2797802_payment-download-png-image-pagamento-recorrente-transparent-png.png',
                    'qtyp' => $this->qtyp,
                    'datetime' => $this->datetime,
                    'payment' => 1,
                    'status' => 1,
                ] );
            }
        } else {
            if ( $forCheck >= 1 ) {
                $order = DB::table( 'preorder' )->insertGetId( [
                    'customer_id' => auth()->user()->id,
                    'code' =>'OD-'. rand( 1000, 9999 ),
                    'subtotal' => str_replace( ',', '', Cart::subTotal() ),
                    'total' => str_replace( ',', '', Cart::subTotal() ),
                    'onepay_image' => 'https://www.pngitem.com/pimgs/m/279-2797802_payment-download-png-image-pagamento-recorrente-transparent-png.png',
                    'qtyp' => $this->qtyp,
                    'datetime' => $this->datetime,
                    'payment' => 1,
                    'status' => 1,
                ] );
            } else {
                $order = DB::table( 'preorder' )->insertGetId( [
                    'customer_id' => auth()->user()->id,
                    'code' =>'OD-'. rand( 1000, 9999 ),
                    'subtotal' => 50000,
                    'total' => str_replace( ',', '', Cart::subTotal() ),
                    'onepay_image' => 'https://www.pngitem.com/pimgs/m/279-2797802_payment-download-png-image-pagamento-recorrente-transparent-png.png',
                    'qtyp' => $this->qtyp,
                    'datetime' => $this->datetime,
                    'payment' => 1,
                    'status' => 1,
                ] );
            }
        }
        $cartContent = Cart::content();
        foreach ( $cartContent as $item ) {
            if ( isset( $item->id ) ) {
                DB::table( 'preorder_food' )->insert( [
                    'preorder_id' => $order,
                    'foods_id' => $item->id,
                    'food_qty' => $item->qty,
                    'subtotal' => str_replace( ',', '', Cart::subTotal() ),
                ] );
            }
        }
        foreach ( $this->chosetable as $item ) {
            DB::table( 'preorder_table' )->insert( [
                'preorder_id' => $order,
                'tables_id' => $item,
            ] );
        }
        Cart::destroy();
        $this->resetForm();
        toastr()->success( 'successfully!' );
        return redirect()->route( 'history' );
    }

    public function resetForm() {
        $this->chosetable = [];
        $this->qtyp = '';
        $this->datetime = '';
        $this->payment = '';

    }
}