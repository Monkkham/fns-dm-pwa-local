<?php

namespace App\Http\Livewire\Components;

use Livewire\Component;
use Illuminate\Support\Facades\DB;

class AboutContent extends Component
{
    public $abouts;
    public function render()
    {
        $this->abouts = DB::table( 'abouts' )->get();
        return view('livewire.components.about-content')->layout( 'layouts.app' );
    }
}