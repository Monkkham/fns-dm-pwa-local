<?php

namespace App\Http\Livewire\Components;

use Illuminate\Support\Facades\DB;
use Livewire\Component;

class Category extends Component
{
    public $category;
    public function render()
    {
        $this->category = DB::table('food_type')->get();
        return view('livewire.components.category')->layout( 'layouts.auth.style' );
    }
}