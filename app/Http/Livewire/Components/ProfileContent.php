<?php

namespace App\Http\Livewire\Components;

use Livewire\Component;

class ProfileContent extends Component
{
    public function render()
    {
        return view('livewire.components.profile-content')->layout( 'layouts.app' );
    }
}