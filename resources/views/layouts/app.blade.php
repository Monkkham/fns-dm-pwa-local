<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta http-equiv="Content-Security-Policy" content="default-src * 'self' 'unsafe-inline' 'unsafe-eval' data: gap:">
    <link rel="icon" type="image/x-icon" href="images/logo.png">
    <title>{{ env('APP_NAME') }}</title>
    <link href="https://fonts.googleapis.com/css?family=Prompt:300,400,500,600,700,800,900|Playfair+Display:400,700,900"
        rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/framework7.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/framework7-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('css/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">

    <!-- PWA  -->
    <meta name="theme-color" content="#6777ef" />
    <link rel="apple-touch-icon" href="{{ asset('logo.PNG') }}">
    <link rel="manifest" href="{{ asset('/manifest.json') }}">
    <!-- END PWA  -->

    @stack('third_party_stylesheets')

    @stack('page_css')
    @livewireStyles

</head>

<body>
    <div id="app">

        <div class="view view-main view-init ios-edges" data-url="/">
            <div class="page page-home">
                <!-- navbar -->
                @include('layouts.navbar')
                <!-- end navbar -->
                <div class="page-content">
                    {{ $slot }}
                </div>
            </div>
        </div>
    </div>

    <script src="{{ asset('js/framework7.js') }}"></script>
    <script src="{{ asset('js/routes.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>

    <!-- PWA  -->
    <script src="{{ asset('/sw.js') }}"></script>
    <script>
        if (!navigator.serviceWorker.controller) {
            navigator.serviceWorker.register("/sw.js").then(function(reg) {
                console.log("Service worker has been registered for scope: " + reg.scope);
            });
        }
    </script>
    <!-- END PWA  -->

    @stack('third_party_scripts')
    @stack('page_scripts')


    @livewireScripts

    @stack('scripts')
</body>

</html>
