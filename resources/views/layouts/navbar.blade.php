<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/css/fontawesome.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/css/all.min.css" />
<div class="navbar navbar-home">
    <div class="navbar-inner">
        <div class="block">
            <div class="row">
                <div class="col-20">
                    <a onclick="document.location='/'" class="link text-color-white">
                        <i class="ti-home"></i>
                    </a>
                </div>
                <div class="col-20">
                    <a onclick="document.location='/categories'" class="link text-color-white">
                        <i class="ti-view-list-alt"></i>
                    </a>
                </div>
                <div class="col-20">
                    <a onclick="document.location='/abouts'" href="#" class="link text-color-white">
                        <i class="fas fa-address-book"></i>
                    </a>
                </div>
                <div class="col-20">
                    <a onclick="document.location='/history'" class="link">
                        <i class="ti-timer"></i>
                    </a>
                </div>
                <div class="col-20">
                    <a onclick="document.location='/profiles'" class="link text-color-white">
                        <i class="ti-layout-grid2"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
