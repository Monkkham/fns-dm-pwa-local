<div wire:poll>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/css/fontawesome.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/css/all.min.css" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <!-- title header -->
    <div class="page">
        <div class="navbar navbar-page">
            <div class="navbar-inner sliding">
                <div class="left">
                    <a onclick="document.location='/'" class="link back">
                        <i class="ti-arrow-left"></i>
                    </a>
                </div>
                <div class="title">
                    <h3>ກ່ຽວກັບພວກເຮົາ</h3>
                </div>
            </div>
        </div>
        <div class="page-content">
            <!-- categories -->
            <div class="categories segments-page">
                <div class="container">
@foreach ($abouts as $item)
                    <!-- About Me Box -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h4 class="card-title">{!! $item->name !!}</h4>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <strong><i class="fas fa-book mr-1"></i> ຄຳອະທິບາຍ</strong>

                            <p class="text-muted">
                                {!! $item->note !!}
                            </p>

                            <hr>

                            <strong><i class="fas fa-map-marker-alt mr-1"></i> ທີ່ຕັ້ງ</strong>

                            <p class="text-muted">{!! $item->address !!}</p>

                            <hr>

                            <strong><i class="fas fa-pencil-alt mr-1"></i> ເບີໂທ</strong>

                            <p class="text-muted">
                                <span class="tag tag-danger">{{ $item->phone }}</span>
                            </p>

                            <hr>

                            <strong><i class="far fa-file-alt mr-1"></i> ອີເມວ</strong>

                            <p class="text-muted">{{ $item->email }}</p>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    @endforeach
                    <!-- /.card -->
                </div>
            </div>
        </div>
