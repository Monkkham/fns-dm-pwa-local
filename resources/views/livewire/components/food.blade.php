<div wire:poll>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/css/fontawesome.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/css/all.min.css" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <!-- title header -->
    <div class="page">
        <div class="navbar navbar-page">
            <div class="navbar-inner sliding">
                <div class="left">
                    @if ($checkback == 0)
                        <a onclick="document.location='/'" class="link back">
                            <i class="ti-arrow-left"></i>
                        </a>
                    @else
                        <a onclick="document.location='/categories'" class="link back">
                            <i class="ti-arrow-left"></i>
                        </a>
                    @endif
                </div>
                <div class="title">
                    ລາຍການສິນຄ້າ
                </div>
                <div class="right">
                    <a onclick="document.location='/cart'"><i class="ti-shopping-cart-full">{{ $cartCount }}</i></a>
                </div>
            </div>
        </div>
        <div class="page-content">
            <!-- popular menu -->
            <div class="popular-menu segments-page">
                <div class="container">
                    <div class="row">
                        @foreach ($foods as $item)
                        @if ($cartData->where('id', $item->id)->count() > 0)
                        <div class="col-50">
                            <div class="content">
                                <img style="width: 100%; height:260px" src="{{ $item->image }}" alt="">
                                <div class="text" style="padding-bottom: 10px">
                                    <a wire:click='addCart({{ $item->id }})'>
                                        <h4>{{ $item->name }}</h4>
                                    </a>
                                    <p style="padding-bottom: 10px; font-size:18px" class="price">ລາຄາ: {{ number_format($item->price) }} ₭</p>
                                    <button disabled class="btn btn-warning" wire:click='addCart({{ $item->id }})'>
                                          <i class="fas fa-check-circle"></i>  ໃນກະຕ່າ
                                    </button>
                                </div>
                            </div>
                        </div>
                                @else
                                <div class="col-50">
                                    <div class="content">
                                        <img style="width: 100%; height:260px" src="{{ $item->image }}" alt="">
                                        <div class="text" style="padding-bottom: 10px">
                                            <a wire:click='addCart({{ $item->id }})'>
                                                <h4>{{ $item->name }}</h4>
                                            </a>
                                            <p style="padding-bottom: 10px; font-size: 18px" class="price">ລາຄາ: {{ number_format($item->price) }} ₭</p>
                                            <button class="btn btn-success" wire:click='addCart({{ $item->id }})'>
                                                  <i class="fas fa-cart-plus"></i>  ເພີ່ມກະຕ່າ
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                   
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
            <!-- end popular menu -->
        </div>
    </div>

</div>
