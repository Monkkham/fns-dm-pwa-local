<div wire:poll.10s>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/css/fontawesome.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/css/all.min.css" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <div class="page">
        <div class="navbar navbar-page">
            <div class="navbar-inner sliding">
                <div class="left">
                    <a onclick="document.location='/'" class="link back">
                        <i class="ti-arrow-left"></i>
                    </a>
                </div>
                <div class="title">
                    <h6><i class="fas fa-spoon"></i> ຮ້ານອາຫານດາວອັງຄານ ຍິນດີຕ້ອນຮັບ</h6>
                </div>
            </div>
        </div>
        <div class="page-content">
            <!-- login -->
            <div class="login segments-page">
                <div class="container">
                    <div class="logos">
                        <div class="image">
                            <img src="images/logo.png" alt="">
                        </div>
                    </div>
                    <div class="list">
                        <div class="item-input-wrap">
                            <input wire:model='phone' type="number" minlength="8" placeholder="ເບີໂທ 8 ຕົວເລກ"
                                required>
                            @error('phone')
                                <span style="color: red" class="error">{{ $message }}</span>
                            @enderror
                        </div>
                        <br>
                        <div class="item-input-wrap">
                            <input wire:model='password' type="password" minlength="8" placeholder="ລະຫັດຜ່ານ"
                                required>
                            @error('password')
                                <span style="color: red" class="error">{{ $message }}</span>
                            @enderror
                        </div>
                        <br>
                        <button wire:click='login' class="button"><i class="ti-shift-right"></i>ເຂົ້າສູ່ລະບົບ</button>
                        <br>
                        <button onclick="document.location='/register'" class="button bg-success"><i
                                class="fas fa-edit"></i>ລົງທະບຽນ</button>
                    </div>
                    {{-- <div class="login-with">
                        <p>ເຂົ້າສູ່ລະບົບດ້ວຍ</p>
                        <ul>
                            <li><a href="#"><i class="ti-facebook"></i></a></li>
                            <li><a href="#"><i class="ti-twitter"></i></a></li>
                            <li><a href="#"><i class="ti-google"></i></a></li>
                        </ul>
                    </div> --}}
                </div>
            </div>
            <!-- end login -->
        </div>
    </div>

</div>
