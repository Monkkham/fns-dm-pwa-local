<div wire:poll.10s>

    <div class="page">
        <div class="navbar navbar-page">
            <div class="navbar-inner sliding">
                <div class="left">
                    <a onclick="document.location='/'" class="link back">
                        <i class="ti-arrow-left"></i>
                    </a>
                </div>
                <div class="title">
                    <h3>ປະເພດອາຫານ</h3>
                </div>
            </div>
        </div>
        <div class="page-content">
            <!-- categories -->
            <div class="categories segments-page">
                <div class="container">
                    @foreach ($category as $item)
                        <div class="content">
                            <a onclick="document.location='/menu/{{ $item->id }}'">
                                <img src="https://www.eatingwell.com/thmb/m5xUzIOmhWSoXZnY-oZcO9SdArQ=/1500x0/filters:no_upscale():max_bytes(150000):strip_icc()/article_291139_the-top-10-healthiest-foods-for-kids_-02-4b745e57928c4786a61b47d8ba920058.jpg"
                                    alt="">
                                <div class="mask"></div>
                                <div class="title">
                                    <h4 class="text-white">{{ $item->name }}</h4>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
            <!-- end categories -->
        </div>
    </div>

</div>
