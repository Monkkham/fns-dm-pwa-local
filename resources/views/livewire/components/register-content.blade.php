<div wire:poll.10s>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/css/fontawesome.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/css/all.min.css" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <div class="page">
        <div class="navbar navbar-page">
            <div class="navbar-inner sliding">
                <div class="left">
                    <a onclick="document.location='/login'" class="link back">
                        <i class="ti-arrow-left"></i>
                    </a>
                </div>
                <div class="title">
                    <h4>ລົງທະບຽນນຳໃຊ້ລະບົບ</h4>
                </div>
            </div>
        </div>
        <div class="page-content">
            <!-- login -->
            <div class="login segments-page">
                <div class="container">
                    <div class="logos">
                        <div class="image">
                            <img style="width: 70px; height:70px;" src="https://i.pinimg.com/originals/53/c9/28/53c92803ebd4c6fe0f908c6878c1e1da.png" alt="">
                        </div>
                    </div>
                    <div class="list">
                        <div class="item-input-wrap">
                            <input wire:model='name' type="text" placeholder="ຊື່ ນາມສະກຸນ"
                                @error('address') is-invalid @enderror>
                            @error('name')
                                <span style="color: red" class="error">{{ $message }}</span>
                            @enderror
                        </div>
                        <br>
                        <div class="item-input-wrap">
                            <input wire:model='phone' type="number" minlength="11" placeholder="ເບີໂທ: 8 ຕົວເລກ"
                                @error('phone') is-invalid @enderror>
                            @error('phone')
                                <span style="color: red" class="error">{{ $message }}</span>
                            @enderror
                        </div>
                        <br>
                        {{-- <div class="form-group">
                            <label>ເລືອກແຂວງ</label>
                            <select wire:model="province_id" id="select1"
                                class="form-control
                             @error('province_id') is-invalid @enderror">
                                <option value="" selected>{{ __('lang.select') }}</option>
                                @foreach ($province as $item)
                                    <option value="{{ $item->id }}">{{ $item->name_la }}</option>
                                @endforeach
                            </select>
                            @error('province_id')
                                <span style="color: red" class="error">{{ $message }}</span>
                            @enderror
                        </div> --}}
                        <br>
                        <div class="item-input-wrap">
                            <input wire:model='password' type="password" minlength="11" placeholder="ລະຫັດຜ່ານ"
                                @error('password') is-invalid @enderror>
                            @error('password')
                                <span style="color: red" class="error">{{ $message }}</span>
                            @enderror
                        </div>
                        <br>
                        <div class="item-input-wrap">
                            <input wire:model='confirm_password' type="password" minlength="11"
                                placeholder="ຍືນຍັນລະຫັດຜ່ານ" @error('address') is-invalid @enderror>
                            @error('confirm_password')
                                <span style="color: red" class="error">{{ $message }}</span>
                            @enderror
                        </div>
                        <br>
                        <button wire:click='register' class="btn btn-primary"><i class="fas fa-edit"></i>ລົງທະບຽນ</button>
                    </div>
                </div>
            </div>
            <!-- end login -->
        </div>
    </div>

</div>
